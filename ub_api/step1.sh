#! /bin/sh

REDIRECTURI=https://62a5311e-97d5-4ed0-81d2-94046cdd09ac.example.org/redirect
CLIENTID=C7X2MY2NvqNGOwsVYxnZt8Fh_9DY6Obw-t5b58gLlqw=
SECRET=vZmBGEUepomidR_7SbmmPuwi6cx6V4JVLb9UaGxXfqE=


TOKEN=$(curl -s -k -X POST \
     "https://ob.ulster.useinfinite.io/token" \
     -H 'Content-Type: application/x-www-form-urlencoded' \
     -d "grant_type=client_credentials&client_id=$CLIENTID&client_secret=$SECRET&scope=accounts"
 )

ACCESSTOKEN=$(echo $TOKEN | jq -r .access_token)

RESP2=$(curl -s -k -X POST "https://ob.ulster.useinfinite.io/open-banking/v2.0/account-requests" -H "Authorization: Bearer ${ACCESSTOKEN}" -H 'Content-Type: application/json' -H 'x-fapi-financial-id: TBD' -d @permissions.json)

REQID=$(echo $RESP2 | jq -r '.Data.AccountRequestId')


echo "Visit the following URL and authenticate"
echo "the redirect will fail, but the URL will have the account-request-id needed as the argument for step2"
echo "https://api.ulster.useinfinite.io/authorize?client_id=${CLIENTID}&response_type=code+id_token&scope=openid+accounts&redirect_uri=${REDIRECTURI}&request=${REQID}"
