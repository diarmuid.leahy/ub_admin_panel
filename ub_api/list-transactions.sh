#! /bin/sh

ACCESSTOKEN=$(cat step2.token.json | jq -r '.access_token')
ACCOUNTID=$(./account-list.sh | jq -r '.Data.Account[0].AccountId')


curl -k -X GET \
  https://ob.ulster.useinfinite.io/open-banking/v2.0/accounts/${ACCOUNTID}/transactions \
  -H "Authorization: Bearer ${ACCESSTOKEN}" \
  -H 'x-fapi-financial-id: TBD'
